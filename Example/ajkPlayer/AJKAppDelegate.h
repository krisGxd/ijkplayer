//
//  AJKAppDelegate.h
//  ajkPlayer
//
//  Created by Guxiaodong941005 on 12/07/2020.
//  Copyright (c) 2020 Guxiaodong941005. All rights reserved.
//

@import UIKit;

@interface AJKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
