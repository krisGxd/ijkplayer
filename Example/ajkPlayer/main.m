//
//  main.m
//  ajkPlayer
//
//  Created by Guxiaodong941005 on 12/07/2020.
//  Copyright (c) 2020 Guxiaodong941005. All rights reserved.
//

@import UIKit;
#import "AJKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AJKAppDelegate class]));
    }
}
