#
# Be sure to run `pod lib lint ajkPlayer.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ajkPlayer'
  s.version          = '0.0.1'
  s.summary          = 'A short description of ajkPlayer.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/Guxiaodong941005/ajkPlayer'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Guxiaodong941005' => '970768639@qq.com' }
  s.source           = { :git => 'https://github.com/Guxiaodong941005/ajkPlayer.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'
  s.source_files = 'ajkPlayer/Classes/**/*'
  s.requires_arc = true
  s.libraries = 'c++','libbz2','libz','libstdc++',''
  
  s.frameworks = 'UIKit','AudioToolbox','CoreGraphics','CoreMedia','CoreVideo','MediaPlayer','OpenGLES','QuartzCore','VideoToolbox','SystemConfiguration','MobileCoreServices','AVFoundation'
 #依赖自己的或别人的Framework文件
  s.vendored_frameworks = 'IJKMediaFramework.framework'
  s.xcconfig = { 'OTHER_LDFLAGS' => '-ObjC'}
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  
  # s.resource_bundles = {
  #   'ajkPlayer' => ['ajkPlayer/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
